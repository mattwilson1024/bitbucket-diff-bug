// Just a random file to help demonstrate a problem with diffs in Bitbucket

interface IUser {
  id: string;
  name: string;
  age: number;
  favouriteColour: Colour;
}

interface IPet {
  type: `cat`|`dog`|`guineaPig`;
  name: string;
  colour: Colour;
}

enum Colour {
  black,
  blue,
  brown,
  green,
  grey,
  red,
  white,
  yellow,
}